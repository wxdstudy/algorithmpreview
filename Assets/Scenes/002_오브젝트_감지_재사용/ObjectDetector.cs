 using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class ObjectDetector : MonoBehaviour
{
    /// <summary>
    /// 감지 영역의 반지름입니다.
    /// </summary>
    public float m_DetectRadius;

    /// <summary>
    /// 총알 발사 딜레이
    /// </summary>
    public float m_ShotDelay;

    /// <summary>
    /// 발사시킬 총알 프리팹
    /// </summary>
    public ProjectileMovement m_BulletPrefabs;

    /// <summary>
    /// 오브젝트 감지됨을 나타냅니다.
    /// </summary>
    private bool _IsDetected;

    /// <summary>
    /// 마지막 총알 발사 시간을 저장할 변수
    /// </summary>
    private float _LastFireTime;

    // 생성될 총알 객체들이 저장될 풀입니다.
    private List<ProjectileMovement> _ObjectPool = new();



    private void Update()
    {
        // DetectablCube 이름과 일치하는 레이어 인덱스를 얻습니다.
        int layer = LayerMask.NameToLayer("DetectableCube");
        /// NameToLayer(name) : name 과 일치하는 레이어 인덱스를 반환합니다.

        // 지정한 반경내에 감지된 오브젝트를 확인합니다.
        Collider[] collisions = Physics.OverlapSphere(
            transform.position,
            m_DetectRadius,
            1 << layer);

        // 감지된 오브젝트가 존재한다면 
        _IsDetected = collisions.Length > 0;


        // 감지된 오브젝트들을 확인합니다.
        foreach(Collider collider in collisions)
        {
            // 감지된 오브젝트 이름을 출력합니다.
            Debug.Log(collider.gameObject.name);
        }

        // 감지 시 총알 발사
        if (_IsDetected)
        {
            Time.timeScale = 0.1f;

            // m_ShotDelay 만큼 시간이 지났다면
            if (Time.time - _LastFireTime > m_ShotDelay)
            {
                // 발사시킨 시간을 기록합니다.
                _LastFireTime = Time.time;
                
                // 방향을 얻습니다.
                Vector3 direction = (collisions[0].transform.position - transform.position).normalized;

                // 총알 발사
                ShotBullet(direction);
            }
        }
        else
        {
            Time.timeScale = 1.0f;
        }
    }

    /// <summary>
    /// 총알을 발사시킵니다.
    /// </summary>
    /// <param name="direction"></param>
    private void ShotBullet(Vector3 direction)
    {
        // 재사용 가능한 오브젝트를 얻습니다.
        ProjectileMovement projectile = GetPoolObject();
        
        // 재사용 가능한 오브젝트가 존재하지 않는다면
        if (projectile == null)
        {
            projectile = Instantiate(m_BulletPrefabs);
            // 생성된 오브젝트를 풀에 등록합니다.
            AddPoolObject(projectile);
        }
        else
        {
            projectile.gameObject.SetActive(true);
        }

        projectile.transform.position = transform.position;
        projectile.m_Direction = direction;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(
            transform.position,
            m_DetectRadius);
    }

    // 풀에 오브젝트를 등록합니다.
    private void AddPoolObject(ProjectileMovement projectile)
    {
        _ObjectPool.Add(projectile);

    }

    // 등록된 오브젝트 중 재사용 가능한 상태의 오브젝트를 반환합니다.ㄴ
    private ProjectileMovement GetPoolObject()
    {
        return _ObjectPool.Find((ProjectileMovement projectile) =>
            !projectile.gameObject.activeSelf);
    }



}
