using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileMovement : MonoBehaviour
{
    // 날아갈 방향
    public Vector3 m_Direction;

    // 날아갈 속력
    public float m_Speed;

    private void Update()
    {
        MoveProjectile();
    }

    /// <summary>
    /// 발사체를 이동시킵니다.
    /// </summary>
    private void MoveProjectile() 
    {
        // 감지를 위한 구체를 발사시킬 시작점과 방향을 지정합니다.
        Ray ray = new Ray(transform.position, m_Direction);

        // 발사체 이동거리 
        float distance = m_Speed * Time.deltaTime;

        // 감지시킬 오브젝트 레이어
        int layerMask = 1 << LayerMask.NameToLayer("DetectableCube");

        RaycastHit hitResult;

        if (Physics.SphereCast(ray, 0.1f, out hitResult, distance, layerMask)) 
        {
            distance = hitResult.distance;
            // hitResult.distance : 시작 위치부터 감지된 오브젝트 까지의 거리

            // 감지 이벤트를 발생시킵니다.
            CubeMovement cubeMovement =  hitResult.transform.GetComponent<CubeMovement>();
            cubeMovement.OnHit();

            // 이 총알 오브젝트 비활성화
            gameObject.SetActive(false);
        }

        // distance 만큼 m_Direction 방향으로 이동시킵니다.
        transform.position += m_Direction * distance;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(
            transform.position + (m_Direction * m_Speed * Time.deltaTime),
            0.1f);
    }


}
