using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMovement : MonoBehaviour
{
    public float m_MoveSpeed;

    private void Update()
    {
        transform.position -= Vector3.right * m_MoveSpeed * Time.deltaTime;
    }

    public void OnHit()
    {
        Debug.Log("�Ѿ˿� ����!");
    }
}
