using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 플레이어 상태를 나타내기 위한 열거 형식
/// </summary>
public enum PlayerState :sbyte
{
    // 기본 상태
    Default,

    // 건축 상태
    BuildMode
}

public class PlayerController : MonoBehaviour
{
    public Sample001UI m_SampleUI;

    public GameObject m_ObjectPrefab;

    // 현재 상태
    private PlayerState _CurrentState = PlayerState.Default;
    
    // 오브젝트 생성 중 커서 위치를 따라다닐 오브젝트
    private GameObject _CursorTrackingObject;


    private void Awake()
    {
        // UI 버튼 클릭 이벤트에 CreateObject() 메서드 바인딩
        m_SampleUI.onCreateObjectButtonEvent += CreateObject;
    }

    private void Update()
    {
        if (_CurrentState == PlayerState.BuildMode)
        {
            // 감지된 오브젝트의 존재 유무를 확인하기 위한 변수
            bool isHit;

            // 커서 위치를 얻습니다.
            Vector3 cursorWorldPosition = GetMousePositionInWorld(out isHit);

            // 감지된 오브젝트가 존재하는 경우
            if (isHit)
            {
                // 커서 위치로 옮깁니다.
                _CursorTrackingObject.transform.position = cursorWorldPosition;
            }

            // 마우스 왼쪽 버튼 클릭이 이루어졌다면
            if(Input.GetMouseButtonDown(0))
            {
                FinishBuildMode();
            }
        }
    }

    private void CreateObject()
    {
        // Default 상태가 아닌 경우 실행X
        if (_CurrentState != PlayerState.Default) return;

        _CurrentState = PlayerState.BuildMode;


        // 오브젝트 복사 생성
        GameObject newObject = Instantiate(m_ObjectPrefab);
                
        _CursorTrackingObject = newObject;
    }

    // 마우스 위치를 월드 위치로 반환합니다.
    // isHit : 감지된 오브젝트의 존재 유무를 판단합니다.
    private Vector3 GetMousePositionInWorld(out bool isHit)
    {
        // 카메라에서 마우스 위치로 레이를 발사하기 위한 Ray 데이터를 얻습니다.
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        // 감지 결과에 대한 내용을 담기 위한 변수
        RaycastHit hit;

        // 감지된 오브젝트가 존재한다면
        if (Physics.Raycast(ray, out hit)) 
        {
            isHit = true;
            return hit.point;
            // RaycastHit.point : 감지된 위치
        }
        //감지된 오브젝트가 존재하지 않는다면
        else
        {
            isHit = false;
            return Vector3.zero;
        }
    }

    // 건축 모드를 종료합니다.
    private void FinishBuildMode()
    {
        // 기본 모드로 돌아갑니다.
        _CurrentState = PlayerState.Default;

        // 커서를 따라다닐 오브젝트를 비웁니다.
        _CursorTrackingObject = null;
    }
}
