using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sample001UI : MonoBehaviour
{
    public Button m_CreateObjectButton;
    
    /// <summary>
    /// 오브젝트 생성 버튼 이벤트
    /// </summary>
    public System.Action onCreateObjectButtonEvent;


    private void Awake()
    {
        // m_CreateObjectButton 클릭 이벤트에 OnCreateObjectButtonClicked()메서드를 등록합니다.
        m_CreateObjectButton.onClick.AddListener(OnCreateObjectButtonClicked);        
    }

    private void OnCreateObjectButtonClicked()
    {
        onCreateObjectButtonEvent?.Invoke();
    }
}
