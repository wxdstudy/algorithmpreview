using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

public struct PathInfo
{
    // 시작 위치부터 방향
    public Vector2Int directionFromStart;

    // 시작 위치부터 떨어진 거리
    public float distanceFromStart;

    // 도착 노드
    public CubeTile goalNode;
}

public class PathMovement : MonoBehaviour
{
    // 초기 위치입니다.
    private Vector3 _InitialPosition;

    // 이동 목표
    private Vector3 _MoveTarget;

    // 이동 속력
    private float _MoveSpeed = 5.0f;

    private int _TargetPathIndex;
    private Coroutine _UpdateMoveTargetCoroutine;


    private List<PathInfo> _PathInfos;

    private void Start()
    {
        // 코루틴을 시작시킵니다.
        _UpdateMoveTargetCoroutine = StartCoroutine(UpdateMoveTarget());
    }

    private void Update()
    {
        transform.position = Vector3.MoveTowards(
            transform.position,
            _MoveTarget,
            _MoveSpeed * Time.deltaTime);
    }

    // 코루틴
    // Update 와는 다르게 하나의 메서드 안에서 MainThread 제어권을 유니티에 반환하고,
    // 특정 시점에 특정 위치에 있는 구문부터 진행되도록 할 수 있는 기능.
    private IEnumerator UpdateMoveTarget()
    {
        yield return new WaitUntil(() => _PathInfos != null);

        //for (int i = 1; i < _PathInfos.Count; ++i)
        while (_TargetPathIndex < _PathInfos.Count)
        {

            // 목표 위치를 설정합니다.
            _MoveTarget = _PathInfos[_TargetPathIndex].goalNode.transform.position;

            // Vector3.Distance(startPosition, transform.position) : 시작 위치에서 현재 위치까지의 거리
            // _PathInfos[i].distanceFromStart 이전 노드에서 경유지까지의 거리
            // 경유지까지의 거리를 초과할 때까지 대기합니다.
            yield return new WaitUntil(() =>
                Mathf.Approximately(Vector3.Distance(transform.position, _MoveTarget), 0));

            ++_TargetPathIndex;
        }


        /*
		// 다음 Update 까지 대기합니다.
		yield return null;

		// 다음 FixedUpdate 까지 대기합니다.
		yield return new WaitForFixedUpdate();

		// 다음 LateUpdate 까지 대기합니다.
		yield return new WaitForEndOfFrame();

		// 지정한 시간만큼 대기합니다.
		// Timescale 의 영향을 받습니다.
		yield return new WaitForSeconds(10);
		// 지정한 시간만큼 대기합니다.
		// Timescale 의 영향을 받지 않습니다.
		yield return new WaitForSecondsRealtime(10);

		// 조건식을 검사하여 조건식의 결과가 True 가 될 때까지 대기합니다.
		yield return new WaitUntil(() => true);

		// 조건식을 검사하여 조건식의 결과가 False 가 될 때까지 대기합니다.
		yield return new WaitWhile(() => false);
		*/
    }


    // 초기화합니다.
    public void Initialize(Vector3 initialPosition)
    {
        // 초기 위치로 이동시킵니다.
        transform.position = initialPosition;
    }

    public void UpdatePath(List<CubeTile> path)
    {
        List<PathInfo> pathInfos = new();

        // 시작 위치를 나타낼 변수
        Vector3 startPosition = _InitialPosition;

        // 이전 방향을 나타낼 변수
        Vector2Int prevDirection = Vector2Int.zero;

        // 시작 위치부터 떨어진 거리를 기록하기 위한 변수
        float distanceFromStart = 0;

        // 이전 노드를 기록하기 위한 변수
        CubeTile prevNode = path[0];

        for (int i = 0; i < path.Count; ++i)
        {
            // 다음 노드를 얻습니다.
            CubeTile nextNode = path[i];

            // 시작 위치에서 다음 노드로 향하는 방향을 얻습니다.
            Vector2Int dirToNextNode = nextNode.m_NodePosition - prevNode.m_NodePosition;

            // 다음 노드까지의 거리를 계산합니다.
            distanceFromStart = Vector3.Distance(startPosition, nextNode.transform.position);

            // 마지막 노드임을 나타냅니다.
            bool isLastNode = (i == path.Count - 1);

            // 이전 방향과 다를 경우
            if (prevDirection != dirToNextNode ||

                // 마지막 경로 기록을 위한 조건식
                isLastNode)
            {
                PathInfo newPathInfo = new();
                newPathInfo.distanceFromStart = distanceFromStart;
                newPathInfo.directionFromStart = prevDirection;
                newPathInfo.goalNode = isLastNode ? nextNode : prevNode;
                pathInfos.Add(newPathInfo);

                distanceFromStart = 0;
                prevDirection = dirToNextNode;
            }
            prevNode = nextNode;
        }
        
        _PathInfos = pathInfos;
        
        if (_UpdateMoveTargetCoroutine != null)
        {
            StopCoroutine(_UpdateMoveTargetCoroutine);
            transform.position = Vector3.zero;
            _UpdateMoveTargetCoroutine = StartCoroutine(UpdateMoveTarget());
            _TargetPathIndex = 1;
        }
    }



    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        foreach (PathInfo pathInfo in _PathInfos)
        {
            Vector3 nodePosition = pathInfo.goalNode.transform.position;
            Gizmos.DrawLine(nodePosition, nodePosition + (Vector3.up * 2));

            string context = $" dir from start : {pathInfo.directionFromStart}\n" +
                $"dist from start : {pathInfo.distanceFromStart}\n" +
                $"goalNode pos : {pathInfo.goalNode.m_NodePosition}";

            GUIStyle guiStyle = new();
            guiStyle.normal.textColor = Color.black;

            Handles.Label(
                nodePosition + (Vector3.up * 2),
                context, guiStyle);
        }
    }



}
