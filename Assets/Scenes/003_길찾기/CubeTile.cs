using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TileType
{
    None,

    BlockingTile
}

public class CubeTile : MonoBehaviour
{
    public TileType m_TileType;

    public bool m_StartTile;
    public bool m_GoalTile;

    // 노드 위치
    public Vector2Int m_NodePosition;

    // 시작 지점부터 드는 비용
    public int m_CostFromStart;

    // 끝 지점까지 예상되는 비용
    public int m_HeuristicToGoal;

    // 기반 노드
    public CubeTile m_BaseNode;

    public Material m_DefaultMaterial;
    public Material m_BlockMaterial;
    public Material m_PathMaterial;

    private MeshRenderer _MeshRenderer;

    public MapGenerator m_MapGenerator;

    // 총 비용
    public int totalCost => m_CostFromStart + m_HeuristicToGoal;

    private void Awake()
    {
        _MeshRenderer = GetComponent<MeshRenderer>();
    }

    public void SetPath()
    {
        _MeshRenderer.material = m_PathMaterial;
    }

    public void SetDefault()
    {
        _MeshRenderer.material = m_DefaultMaterial;
    }

    public void SetTileType(TileType newTileType)
    {
        
        m_TileType = newTileType;

        Vector3 currentPosition = transform.position;

        if (newTileType == TileType.None)
        {
            currentPosition.y = 0.0f;
            _MeshRenderer.material = m_DefaultMaterial;
        }

        else if (newTileType == TileType.BlockingTile)
        {
            currentPosition.y = 0.5f;
            _MeshRenderer.material = m_BlockMaterial;
        }

        transform.position = currentPosition;

        m_MapGenerator.InitializeBlocks();
        List<CubeTile> optimalPath = m_MapGenerator.GetOptimalPath();
        m_MapGenerator.RestartMovement(optimalPath);

    }




}
