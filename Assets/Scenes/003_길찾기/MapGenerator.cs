using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Tilemaps;

public class MapGenerator : MonoBehaviour
{
    /// <summary>
    /// 맵 크기
    /// </summary>
    public Vector2Int m_MapSize;
    public CubeTile m_MapTilePrefab;

    private List<CubeTile> _CreatedTileMap;

    public PathMovement m_MoveObjectPrefab;
    private PathMovement _PathMovement;

    private void Start()
    {
        GenerateMap();

        InitializeBlocks();
        List<CubeTile> optimalPath = GetOptimalPath();

        _PathMovement = Instantiate(m_MoveObjectPrefab);
        _PathMovement.Initialize(Vector3.zero);
        RestartMovement(optimalPath);
    }

    private void GenerateMap()
    {
        _CreatedTileMap = new();

        for (int y = 0; y < m_MapSize.y; ++y)
        {
            for (int x = 0; x < m_MapSize.x; ++x)
            {
                CubeTile tileMap = Instantiate(m_MapTilePrefab);
                _CreatedTileMap.Add(tileMap);

                tileMap.transform.position =
                    new Vector3(x + (0.01f * x), 0, y + (0.01f * y));

                tileMap.m_MapGenerator = this;
                tileMap.m_NodePosition = new(x, y);
            }
        }

        // 시작 블록, 도착지 블록 설정
        _CreatedTileMap[0].m_StartTile = true;
        _CreatedTileMap[_CreatedTileMap.Count - 1].m_GoalTile = true;
    }

    public void RestartMovement(List<CubeTile> path)
    {
        _PathMovement.UpdatePath(path);
    }

    public void InitializeBlocks()
    {
        foreach (CubeTile tile in _CreatedTileMap)
        {
            if (tile.m_TileType == TileType.None)
            {
                tile.SetDefault();
            }
        }
    }

    public List<CubeTile> GetOptimalPath()
    {
        // 열린 목록
        List<CubeTile> openNode = new();

        // 닫힌 목록
        List<CubeTile> closeNode = new();

        // 시작 지점 노드
        CubeTile startNode = _CreatedTileMap[0];

        // 도착 지점 노드
        CubeTile goalNode = _CreatedTileMap[_CreatedTileMap.Count - 1];

        // 시작 지점을 열린 목록에 추가
        openNode.Add(startNode);

        while (openNode.Count > 0)
        {
            // 열린 목록의 첫 번째 요소를 현재 노드로 설정합니다.
            CubeTile currentNode = openNode[0];

            // 만약 열린 목록에 여러 후보 노드들이 존재한다면 가장 효율적인 노드를 선택합니다.
            for (int i = 0; i < openNode.Count; ++i)
            {
                // 후보 노드 중 현재 노드보다 총 비용이 적게 드는 노드를 찾았거나
                if (openNode[i].totalCost < currentNode.totalCost ||

                    // 후보 노드 중 노드의 예상되는 총 비용이 동일하지만
                    (openNode[i].totalCost == currentNode.totalCost &&

                    // 목표까지 예상되는 비용이 더 적은 노드를 찾았다면
                    openNode[i].m_HeuristicToGoal < currentNode.m_HeuristicToGoal))
                {
                    // 해당 후보 노드를 현재 노드로 설정합니다.
                    currentNode = openNode[i];
                }
            }

            // 열린 목록에서 현재 선택된 노드를 제거하며, 닫힌 노드에 추가합니다.
            openNode.Remove(currentNode);
            closeNode.Add(currentNode);

            // 현재 노드가 도착지 노드를 나타내는지 확인합니다.
            if (currentNode.m_NodePosition == goalNode.m_NodePosition)
            {
                // 반환 시킬 경로 리스트
                List<CubeTile> path = new();

                // 도착지 노드의 기반 노드들을 거슬러 올라가며 경로를 완성합니다.
                CubeTile pathNode = goalNode;

                while (pathNode != null)
                {
                    pathNode.SetPath();

                    path.Insert(0, pathNode);
                    pathNode = pathNode.m_BaseNode;
                }

                // 도착지를 찾았기 때문에 탐색 종료.
                return path;
            }

            // 현재 노드의 이웃 노드들을 모두 얻습니다.
            foreach (CubeTile neighbor in GetAdjacentNodes(currentNode))
            {
                // 닫힌 목록에 추가된 노드라면 검사하지 않습니다.
                if (closeNode.Contains(neighbor)) continue;

                // 열린 노드에 추가되지 않은 이웃 노드라면
                if (!openNode.Contains(neighbor))
                {
                    // 기반 노드를 현재 노드로 설정합니다.
                    neighbor.m_BaseNode = currentNode;

                    // 시작 노드에서 드는 비용을 계산합니다.
                    neighbor.m_CostFromStart = currentNode.m_CostFromStart + 10;

                    // 목표 위치까지 예상되는 거리를 계산합니다.
                    neighbor.m_HeuristicToGoal = GetHeuristic(neighbor, currentNode);

                    // 열린 목록에 추가
                    openNode.Add(neighbor);
                }
            }
        }



        return null;
    }

    /// <summary>
    /// 두 노드 사이의 예상 비용을 계산합니다.
    /// </summary>
    /// <param name="a">노드1을 전달합니다.</param>
    /// <param name="b">노드2를 전달합니다.</param>
    /// <returns></returns>
    private int GetHeuristic(CubeTile a, CubeTile b)
    {
        return Mathf.Abs(a.m_NodePosition.x - b.m_NodePosition.x) +
            Mathf.Abs(a.m_NodePosition.y - b.m_NodePosition.y);
    }

    /// <summary>
    /// 기준 노드의 유효한 이웃 노드들을 찾아 반환합니다.
    /// </summary>
    /// <param name="pivotNode">기준 노드를 전달합니다.</param>
    /// <returns></returns>
    private List<CubeTile> GetAdjacentNodes(CubeTile pivotNode)
    {
        // 확인시킬 방향들을 나열합니다.
        Vector2Int[] checkDirections = {
            Vector2Int.left,
            Vector2Int.up,
            Vector2Int.right,
            Vector2Int.down
        };

        // 이웃 노드들을 저장해둘 리스트
        List<CubeTile> neighbors = new();

        foreach (Vector2Int checkDirection in checkDirections)
        {
            // 검사시킬 노드 위치를 얻습니다.
            Vector2Int checkPosition = pivotNode.m_NodePosition + checkDirection;

            // 유효하지 않은 노드라면 확인하지 않습니다.
            if (!IsValidNode(checkPosition)) continue;

            // 검사하는 위치의 CubeTile 객체를 얻습니다.
            CubeTile neighbor = GetCubeTileFromPosition(checkPosition);

            // 이동 불가능한 노드라면 확인하지 않습니다.
            if (neighbor.m_TileType == TileType.BlockingTile) continue;

            // 이웃 노드로 추가합니다.
            neighbors.Add(neighbor);
        }

        // 이웃 노드들을 반환합니다.
        return neighbors;
    }


    /// <summary>
    /// 유효한 노드인지 확인합니다.
    /// </summary>
    /// <param name="nodePosition">확인시킬 위치를 전달합니다.</param>
    /// <returns></returns>
    private bool IsValidNode(Vector2Int nodePosition)
    {
        if (nodePosition.x < 0) return false;
        else if (nodePosition.y < 0) return false;
        else if (nodePosition.x >= m_MapSize.x) return false;
        else if (nodePosition.y >= m_MapSize.y) return false;

        return true;
    }


    /// <summary>
    /// 특정 위치에 해당하는 CubeTile 객체를 반환합니다.
    /// </summary>
    /// <param name="nodePosition">찾고자 하는 CubeTile 객체의 노드 위치를 전달합니다.</param>
    /// <returns>nodePosition 에 해당하는 CubeTile 객체를 반환합니다.</returns>
    private CubeTile GetCubeTileFromPosition(Vector2Int nodePosition)
    {
        return _CreatedTileMap.Find(
            (CubeTile cubeTile) => cubeTile.m_NodePosition == nodePosition);
    }


}
