#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;



[CustomEditor(typeof(CubeTile))]
public class CubeTile_CustomInspector : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        // target : 확장시키려는 형식에 대한 객체
        CubeTile cubeTileComponent = target as CubeTile;

        string buttonLabel = null;
        if (cubeTileComponent.m_TileType == TileType.None)
            buttonLabel = "장애물 블록으로 설정";
        else if (cubeTileComponent.m_TileType == TileType.BlockingTile)
            buttonLabel = "일반 블록으로 설정";

        if (GUILayout.Button(buttonLabel))
        {
            cubeTileComponent.SetTileType(
                cubeTileComponent.m_TileType == TileType.None ?
                TileType.BlockingTile : TileType.None);
        }




    }

}

#endif
